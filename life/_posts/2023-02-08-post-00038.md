---
layout: post
title: "오늘의 커뮤니티별 베스트글 20230129"
toc: true
---


## 오늘하루 부서 핫했던 핫이슈들을 모아보았습니다.
 

 커뮤니티
82쿡,보배드림,클리앙,디시인사이드,딴지일보,이토랜드,에펨코리아,가생이닷컴,웃긴대학,일베저장소,인스티즈,인벤,엠엘비파크,네이트판,뽐뿌,더쿠,루리웹,에스엘알클럽,오늘의유머,와이고수
 

 

 

#에스엘알클럽 (조회수: 1,006,768, 추천수: 543, 댓글수: 5,284)
- 중제 난리난 세종시 근황.jpg (16,303/0/50)
  http://www.slrclub.com/bbs/vx2.php?id=best_article&no=505504
- 붕지 부부랑 빈틈 (15,950/1/75)
  http://www.slrclub.com/bbs/vx2.php?id=best_article&no=505557
- 하니 레전드 몸매 ㄷㄷㄷㄷㄷㄷㄷㄷ (15,152/1/29)
  http://www.slrclub.com/bbs/vx2.php?id=best_article&no=505576
- 제네시스 X 양산 ㄷ ㄷ (14,931/0/64)
  http://www.slrclub.com/bbs/vx2.php?id=best_article&no=505500
- 밸리댄스 누나. GIF (14,597/4/29)
  http://www.slrclub.com/bbs/vx2.php?id=best_article&no=505518

 

#디시인사이드 (조회수: 2,722,137, 추천수: 36,186, 댓글수: 40,328)
- [야갤] 한녀와의 야스 후머리 ㅗㅜㅑ jpg (93,529/1,627/518)
  http://gall.dcinside.com/board/view/?id=dcbest&no=111121&list_num=100&_dcbest=1&page=1
- [싱갤] 오싹오싹 장례식장에서 하면 안되는 일.jpg (90,442/849/380)
  http://gall.dcinside.com/board/view/?id=dcbest&no=111133&list_num=100&_dcbest=1&page=1
- [주갤] 남친 핸드폰 은근히 더더욱 대형사고 친 인티녀 (76,050/566/455)
  http://gall.dcinside.com/board/view/?id=dcbest&no=111119&list_num=100&_dcbest=1&page=1
- [야갤] 충격.. 초토화.. 보이스피싱 박살 근황 ㄷㄷㄷ.JPG (70,921/3,324/1,309)
  http://gall.dcinside.com/board/view/?id=dcbest&no=111166&list_num=100&_dcbest=1&page=1
- [싱갤] 훌쩍훌쩍 저간 일본에서 늘어나고 있다는 부류 (70,436/575/2,006)
  http://gall.dcinside.com/board/view/?id=dcbest&no=111193&list_num=100&_dcbest=1&page=1

 

#네이트판 (조회수: 5,361,807, 추천수: 19,911, 댓글수: 9,178)
- 며늘아기야.. 이리 살짝 와보거라 (193,495/1,765/165)
  http://pann.nate.com/talk/369027565
- 인스타 존잘남의 재물 (187,985/249/96)
  http://pann.nate.com/talk/369031296
- 정국 이유비 (166,733/301/417)
  http://pann.nate.com/talk/369021198
- 네? 이만희에 신천지요 (147,501/142/165)
  http://pann.nate.com/talk/369022298
- 파혼 사후 지팔지꼴녀의 고구마 (147,305/35/144)
  http://pann.nate.com/talk/369023764
(+ 132 건)
 

#루리웹 (조회수: 3,648,908, 추천수: 15,500, 댓글수: 6,407)
- 생각 명절 특수를 노리고 개봉했던 영화들 근황... (57,741/199/164)
  http://bbs.ruliweb.com/best/board/300143/read/60199394?orderby=regdate&range=24h&view=list&m=humor&t=hit
- 웅준 사람 3세대가 오염된 물체 (57,725/205/76)
  http://bbs.ruliweb.com/best/board/300143/read/60200822?orderby=regdate&range=24h&view=list&m=humor&t=hit
- 죽음의 효력 섹1스.jpg (56,361/173/51)
  http://bbs.ruliweb.com/best/board/300143/read/60199728?orderby=regdate&range=24h&view=list&m=humor&t=hit
- ㅎㅂ) 수수한 대안 (46,057/123/42)
  http://bbs.ruliweb.com/best/board/300143/read/60199415?orderby=regdate&range=24h&view=list&m=humor&t=hit
- 반지하 살던 시기 잊히지 않는 기억 (45,243/210/33)
  http://bbs.ruliweb.com/best/board/300143/read/60199072?orderby=regdate&range=24h&view=list&m=humor&t=hit

 

#뽐뿌 (조회수: 2,443,490, 추천수: 1,377, 댓글수: 5,902)
- 어제자 로또 대박난 연유 ㄷ..JPG (53,943/0/56)
  http://www.ppomppu.co.kr/zboard/zboard.php?id=freeboard&no=8204320
- 영혼 낭낭한 어제자 김정현 제작발표회 태도..JPG (48,997/4/105)
  http://www.ppomppu.co.kr/zboard/zboard.php?id=freeboard&no=8204014
- 조금씩 없어지는 추세인 자영업 안 하나..JPG (48,990/1/66)
  http://www.ppomppu.co.kr/zboard/zboard.php?id=freeboard&no=8204201
- 장도연이 사연 실조 다리파 않기 위해 하는것 jpg (45,666/15/61)
  http://www.ppomppu.co.kr/zboard/zboard.php?id=freeboard&no=8203659
- 비행기 날짜 착각해서 퍼스트 클래스 탄 후기.jpg (44,872/1/102)
  http://www.ppomppu.co.kr/zboard/zboard.php?id=freeboard&no=8204255
(+ 74 건)
 

#엠엘비파크 (조회수: 776,182, 추천수: 247, 댓글수: 3,999)
- 오늘자 정우영의 충격적인 슈팅.gif (30,091/0/28)
  http://mlbpark.donga.com/mp/b.php?b=bullpen&id=202301290077983875&m=view
- 가운데 사람들이 지괴 못내려가는 이유.jpg (29,328/1/219)
  http://mlbpark.donga.com/mp/b.php?b=bullpen&id=202301280077977617&m=view
- 46세 아재에게 소개팅 들어오는 요조숙녀 수준.jpg (29,290/4/36)
  http://mlbpark.donga.com/mp/b.php?b=bullpen&id=202301290077984790&m=view
- 보아 최신근황ㄷㄷ (28,553/1/101)
  http://mlbpark.donga.com/mp/b.php?b=bullpen&id=202301280077974341&m=view
- 에일리...노브라 실연 논란 ㄷㄷ. gif (28,488/10/44)
  http://mlbpark.donga.com/mp/b.php?b=bullpen&id=202301280077977260&m=view

 

#인벤 (조회수: 52,763, 추천수: 126, 댓글수: 125)
- 남다른 골반핏 짧은치마 다현 (14,005/17/17)
  http://www.inven.co.kr/board/webzine/2097/2011760?iskin=webzine
- 왜국 트위터에서 욕먹고 있는 아이돌 (9,304/12/16)
  http://www.inven.co.kr/board/webzine/2097/2011741?iskin=webzine
- 희곡 글로리보다 더한 실상 (8,165/29/36)
  http://www.inven.co.kr/board/webzine/2097/2011716?iskin=webzine
- 근자 구글 검색이 이상한 사이트로 오염되는 낙 (7,982/13/17)
  http://www.inven.co.kr/board/webzine/2097/2011785?iskin=webzine
- 오늘자 손흥민 골 궤적 (5,250/16/20)
  http://www.inven.co.kr/board/webzine/2097/2011966?iskin=webzine

 

#에펨코리아 (조회수: 12,186,681, 추천수: 53,795, 댓글수: 17,572)
- 와이프랑 뜨밤 중인데 5살 딸이 목격함 (504,322/2,057/335)
  http://www.fmkorea.com/index.php?mid=best2&listStyle=list&document_srl=5443025012
- 조나단 의외의 근황 ㄷㄷ.jpg (480,713/1,666/332)
  http://www.fmkorea.com/index.php?mid=best2&listStyle=list&document_srl=5443045382
- 복싱에서 국적이 존나 중요한 이유....jpg (474,206/1,866/435)
  http://www.fmkorea.com/index.php?mid=best2&listStyle=list&document_srl=5349318469
- 비행기 날짜 착각해서 퍼스트 클래스 탄 후기.jpg (457,379/1,743/562)
  http://www.fmkorea.com/index.php?mid=best2&listStyle=list&document_srl=5442273492
- 우정잉 보고 주야장천 여우같다고 하는 탁사 (398,863/2,094/735)
  http://www.fmkorea.com/index.php?mid=best2&listStyle=list&document_srl=5443407970
(+ 52 건)
 

#클리앙 (조회수: 2,594,929, 추천수: 9,151, 댓글수: 4,591)
- 김만배 압박해 받은 100억으로 셈 건물 (45,900/342/27)
  http://www.clien.net/service/board/park/17874975?type=recommend
- 3800만원 맹지를 640억원으로 껑충 뛰게 하는 방법이 궁금 (41,600/365/41)
  http://www.clien.net/service/board/park/17874970?type=recommend
- 오지랖도 이정도면 정신병.jpg (40,200/121/253)
  http://www.clien.net/service/board/park/17875081?type=recommend
- 세계적인 조롱거리인 굥 거늬부부 (39,400/132/28)
  http://www.clien.net/service/board/park/17875000?type=recommend
- 설 고성대명 불화 근황.jpg (37,200/111/43)
  http://www.clien.net/service/board/park/17874156?type=recommend

 

#더쿠 (조회수: 2,894,090, 댓글수: 25,575)
- “집 담론 [seoul male breast reduction](https://www.businesswire.com/news/home/20221006005456/en/Trueman-Man-Clinic-Network-Demonstrates-Its-Excellence-in-Gynecomastia-Surgery-in-Korea-with-25700-Operations) 그만하자” 광규형 절규…12억 송도 대장 아파트, 반토막 (110,000/594)
  http://theqoo.net/index.php?mid=hot&document_srl=2705318188
- 곽튜브 보면서 처음으로 실망한 순간... (110,000/533)
  http://theqoo.net/index.php?mid=hot&document_srl=2705478620
- 조금씩 없어지는 추세인 자영업 허리 단특 (99,000/268)
  http://theqoo.net/index.php?mid=hot&document_srl=2705355079
- 싸울때 자리를 피하는 이유.jpg (89,000/539)
  http://theqoo.net/index.php?mid=hot&document_srl=2705437411
- 인제 뭔가 이상한 유플러스 와이파이 (실시간) (87,000/178)
  http://theqoo.net/index.php?mid=hot&document_srl=2705427337

 

#이토랜드 (조회수: 886,456, 추천수: 4,588, 댓글수: 2,125)
- 청담동 미용실 근황....jpg (44,970/127/46)
  http://www.etoland.co.kr/bbs/./board.php?bo_table=etohumor05&wr_id=3003777&is_hit=yes&sca=%C0%AF%B8%D3&page=1
- 46세된 여배우 근황 (37,808/127/53)
  http://www.etoland.co.kr/bbs/./board.php?bo_table=etohumor05&wr_id=2993236&is_hit=yes&sca=%C0%AF%B8%D3&page=1
- 이쁜데 은근 인간성 없는 부녀 특징..JPG (37,359/140/30)
  http://www.etoland.co.kr/bbs/./board.php?bo_table=etohumor05&wr_id=3002900&is_hit=yes&sca=%B1%E2%C5%B8&page=1
- 워터파크된 아파트 리뷰 실시간상황.gif (32,189/85/41)
  http://www.etoland.co.kr/bbs/./board.php?bo_table=etohumor05&wr_id=3007790&is_hit=yes&sca=%B1%E2%C5%B8&page=1
- 상견례 3분만에 파혼한 서방 (30,997/207/104)
  http://www.etoland.co.kr/bbs/./board.php?bo_table=etohumor05&wr_id=3007742&is_hit=yes&sca=%B1%E2%C5%B8&page=1

 

#웃긴대학 (조회수: 1,678,772, 추천수: 24,518, 댓글수: 2,281)
- 요즘은,,4050대가.대세라면서요,,? (45,170/795/68)
  http://web.humoruniv.com/board/humor/read.html?table=pick&pg=0&number=1210433
- 남편이 둥둥이 냉장고 사자는 거 반대한 이유.jpg (39,617/728/43)
  http://web.humoruniv.com/board/humor/read.html?table=pick&pg=0&number=1210471
- 모텔에서 실시간 신음소리 첨 들어보는데 (39,150/477/34)
  http://web.humoruniv.com/board/humor/read.html?table=pick&pg=0&number=1210391
- 서비스 먹튀가 불가능한 업종.jpg (36,566/562/32)
  http://web.humoruniv.com/board/humor/read.html?table=pick&pg=0&number=1210389
- 결혼이 하고 싶다는 기안84 (36,054/619/16)
  http://web.humoruniv.com/board/humor/read.html?table=pick&pg=0&number=1210409
(+ 62 건)
 

#보배드림 (조회수: 1,133,845, 추천수: 20,063, 댓글수: 2,179)
- 미리 6명째 입적 (61,654/795/75)
  http://www.bobaedream.co.kr/view?code=best&No=601775&vdate=h
- 오뚜기 근황 (57,902/985/103)
  http://www.bobaedream.co.kr/view?code=best&No=601820&vdate=h
- 제약회사 영업사원과 의사의 불륜 (47,306/962/0)
  http://www.bobaedream.co.kr/view?code=best&No=601757&vdate=h
- 롯데월드 어드벤처 부산 절대로 가지마세요.... (36,817/628/0)
  http://www.bobaedream.co.kr/view?code=best&No=601759&vdate=h
- 천만뜻밖 만드는 나라가 그다지 없다는 사물 (35,513/430/90)
  http://www.bobaedream.co.kr/view?code=best&No=601793&vdate=h

 

#인스티즈 (조회수: 718,027, 추천수: 547, 댓글수: 4,923)
- 성관계를 조심해야 하는 사태 .feat 대한민국 (114,689/79/368)
  http://www.instiz.net/pt/7315651?green=1
- 백인 친구를 만나서 들은 충격적인 진상 (86,554/36/285)
  http://www.instiz.net/pt/7315711?green=1
- 요정 : 하지 mbti에서 이득 직업을 가지면 월급 730만원(세후) (79,740/27/545)
  http://www.instiz.net/pt/7315564?green=1
- 여의히 모르는 채로 스트레이키즈 현진을 비판하면 합부인 되는 이유 (71,971/97/331)
  http://www.instiz.net/pt/7315741?green=1
- 곽튜브 보면서 처음으로 실망한 순간... (68,686/33/175)
  http://www.instiz.net/pt/7315580?green=1

 

#오늘의유머 (조회수: 100,576, 추천수: 1,334, 댓글수: 314)
- 매운 짬뽕먹고 방명록 남긴 어느 부산소녀 (11,022/79/18)
  http://www.todayhumor.co.kr/board/view.php?table=bestofbest&no=464842&s_no=464842&page=1
- 주차 개같이 해놔도 차빼라고 전화하기 꺼려지는 간극 (10,146/79/17)
  http://www.todayhumor.co.kr/board/view.php?table=bestofbest&no=464843&s_no=464843&page=1
- 하얀 소복입고 뛰는 한국인 아님 (8,754/71/15)
  http://www.todayhumor.co.kr/board/view.php?table=bestofbest&no=464844&s_no=464844&page=1
- 실상 오징어게임 근황 (8,443/71/8)
  http://www.todayhumor.co.kr/board/view.php?table=bestofbest&no=464839&s_no=464839&page=1
- 난방비 급증원인 이와 같이 알자는 빨갱이 (6,890/104/40)
  http://www.todayhumor.co.kr/board/view.php?table=bestofbest&no=464840&s_no=464840&page=1

 

#와이고수 (조회수: 161,529, 추천수: 286, 댓글수: 414)
- 시집살이 논란의 연기파 여배우 (18,044/17/14)
  http://ygosu.com/community/best_article/yeobgi/2015471/?type=daily&type2=common&sdate=2023-01-29&frombest=Y&page=0
- 전왕 Rpg 경합 사기법.gif (14,148/9/43)
  http://ygosu.com/community/best_article/yeobgi/2015500/?type=daily&type2=common&sdate=2023-01-29&frombest=Y&page=0
- 의사를 당황시킨 여유증 중관 (13,440/12/43)
  http://ygosu.com/community/best_article/yeobgi/2015507/?type=daily&type2=common&sdate=2023-01-29&frombest=Y&page=0
- 세계로 퍼져나가는 중국 제도 (11,964/16/14)
  http://ygosu.com/community/best_article/yeobgi/2015481/?type=daily&type2=common&sdate=2023-01-29&frombest=Y&page=0
- [고화질] 손흥민 시즌 8호골 (11,775/23/11)
  http://ygosu.com/community/best_article/soccer/561007/?type=daily&type2=common&sdate=2023-01-29&frombest=Y&page=0

 

#82쿡 (조회수: 415,280, 댓글수: 1,326)
- 가방사러 백화점 갔다가 하여간에 왔어요ㅠ (28,989/105)
  http://www.82cook.com/entiz/read.php?num=3591211
- 떨어지던 아파트가 새로 오르네요 ㅎ (23,957/76)
  http://www.82cook.com/entiz/read.php?num=3591366
- 전세집 보고 온 강아지 왈~~~ (23,854/28)
  http://www.82cook.com/entiz/read.php?num=3591395
- 19금 남자들이 삽입했을 때의 느낌이요 (23,308/38)
  http://www.82cook.com/entiz/read.php?num=3591558
- 집에서 약밥 하지마세요 (21,126/67)
  http://www.82cook.com/entiz/read.php?num=3591029

 

#딴지일보 (조회수: 137,382, 추천수: 2,477, 댓글수: 398)
- 대박! 문동은 기자. ”명신아~~“ 최고네요. (15,793/227/20)
  http://www.ddanzi.com/index.php?mid=free&page=1&document_srl=762791384&statusList=BEST%2CHOTBEST%2CBESTAC%2CHOTBESTAC
- 대한민국 노인들 근황.jpg (13,195/141/82)
  http://www.ddanzi.com/index.php?mid=free&page=1&document_srl=762813402&statusList=BEST%2CHOTBEST%2CBESTAC%2CHOTBESTAC
- 배우 문성근 트위터......ㄷㄷㄷㄷ (12,858/178/3)
  http://www.ddanzi.com/index.php?mid=free&page=1&document_srl=762812405&statusList=BEST%2CHOTBEST%2CBESTAC%2CHOTBESTAC
- 김만배 압박해 받은 100억으로 경도 건물 (12,371/138/20)
  http://www.ddanzi.com/index.php?mid=free&page=1&document_srl=762800923&statusList=BEST%2CHOTBEST%2CBESTAC%2CHOTBESTAC
- 아바타 여주인공의 '일본은 사과하라' (10,735/141/24)
  http://www.ddanzi.com/index.php?mid=free&page=1&document_srl=762822372&statusList=BEST%2CHOTBEST%2CBESTAC%2CHOTBESTAC

 

#가생이닷컴 (조회수: 41,570, 댓글수: 58)
- [JP] 손흥민 시즌 7,8호 멀티골 작렬! 일본반응 (12,869/17)
  http://www.gasengi.com./main/board.php?bo_table=sports&wr_id=415259
- [UK] 손흥민 멀티골! 토트넘 팬 "손흥민이 원래대로 돌아왔다" (10,559/16)
  http://www.gasengi.com./main/board.php?bo_table=sports&wr_id=415266
- [UK] 손흥민 시즌 7,8호 골, 멀티골 작렬! 토트넘 팬 실황반응 (8,863/9)
  http://www.gasengi.com./main/board.php?bo_table=sports&wr_id=415287
- [EU] 무리뉴 감찰 "토트넘에 있을때 김민재를 원했다" 해외반응 (7,312/13)
  http://www.gasengi.com./main/board.php?bo_table=sports&wr_id=415299
- [JP] 한국, 수직이착륙 개인항공기 국산화에 노력, 일본반응 (1,967/3)
  http://www.gasengi.com./main/board.php?bo_table=politics&wr_id=175905
